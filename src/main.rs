use std::io;

fn main() {

    loop{
        println!("Enter an integer number:");

        let mut input  = String::new();

        io::stdin().read_line(&mut input)
            .expect("Failed to read line");

        let input: u64 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("fibs({}) = {}", input, fibs(input));
        println!("fibr({}) = {}", input, fibr(input));
        break;
    }    
}

fn fibr(n: u64) -> u64 {

    if n == 0 {
        return 1
    } else if n == 1 {
        return 1
    } else {
        return fibr(n-1) + fibr(n-2)
    }
}

fn fibs(n: u64) -> u64 {

    if n == 0 {
        return 1;
    } else if n == 1 {
        return 1;
    } else {

        let mut fm1 = 1;
        let mut fm2 = 2;
        let mut f = 0; 

        for _ in 2..n {
            f = fm2 + fm1;
            fm1 = fm2;
            fm2 = f;
        }

    return f;
    }
}
